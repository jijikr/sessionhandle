<html>
	<head> </head>
	<body>
		<div style="text-align:center">
			<?php if(isset(Yii::app()->session['user'])){ ?>
				<a href=<?php echo Yii::app()->createUrl('User/Logout') ?>><button type="button">Logout</button></a>
				<?php }
				else{
					?>
					<a href=<?php echo Yii::app()->createUrl('User/Login') ?>><button type="button">Login</button></a>
					<?php }?>
			<a href=<?php echo Yii::app()->createUrl('User/Signup') ?>><button type="button">Sign Up</button></a>
			<a href=<?php echo Yii::app()->createUrl('User/ListSession')?>><button type="button">List Sessions</button></a>
		</div>

		<?php if(Yii::app()->user->hasFlash("updatemsg")): ?>

			<div class="flash-success" style="font-size:18px;color:#EA235B;">
			  <?php echo Yii::app()->user->getFlash("updatemsg"); ?>
			  <script type="text/javascript">
			  $('.flash-success').animate({opacity: 1.0}, 5000).fadeOut('slow');
			  </script>
			</div>
			<?php endif; ?>
	</body>
</html>