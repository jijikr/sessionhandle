<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */


 if(Yii::app()->user->hasFlash("updatemsg")): ?>

<div class="flash-success" style="font-size:18px;color:#EA235B;">
  <?php echo Yii::app()->user->getFlash("updatemsg"); ?>
  <script type="text/javascript">
  $('.flash-success').animate({opacity: 1.0}, 5000).fadeOut('slow');
  </script>
</div>
<?php endif; 
$this->pageTitle=Yii::app()->name . ' - Sign up';
$this->breadcrumbs=array(
	'Sign up',
);
?>

<h1>Sign up</h1>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'signup-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'confirmPassword'); ?>
		<?php echo $form->passwordField($model,'confirmPassword'); ?>
		<?php echo $form->error($model,'confirmPassword'); ?>
		
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Sign Up'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
