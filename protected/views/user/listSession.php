<?php 
$this->pageTitle=Yii::app()->name . ' - List Sessions';
$this->breadcrumbs=array(
	'List Sessions',
);
?>
<html>
	<head></head>
	<body>
		<div style="text-align:left"><a href=<?php echo Yii::app()->createUrl('User/clearSession')?>>Clear All</a> Sessions for <?php echo $email;?></div> <br>
		<table class="table">
			<thead>
				<tr>
				<th></th>
					<th>Session ID</th>
					<th>OS</th>
					<th>Ip Adress</th>
					<th>Clear</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($userSessions as $session) {?>
				<tr>
				<th><?php if(session_id() == $session->session_id) {
					echo "Current Session ID";
				} else{
						echo ""; }?></th>
					<th > <?php echo $session->session_id; ?></th>
					<th > <?php echo $session->os; ?></th>
					<th > <?php echo $session->ip_address; ?></th>
					<th><a href=<?php echo Yii::app()->createUrl('User/clearSession',array("session_id"=>$session->session_id))?>> Clear</a></th>
				</tr>
			<?php }?>
		</tbody>
		</table>
	</body>
</html>