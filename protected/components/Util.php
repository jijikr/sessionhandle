<?php 
	Yii::import('application.components.*');
	class Util {

		public static function registerUser($model) {
			$msg = "";
			$isUserRegisterd = Util::checkUserRegisterd($model->email);
			if($isUserRegisterd==false) {
				if(trim($model->password) == trim($model->confirmPassword)) {
					$user = new User;
					$user->email = $model->email;
					$user->password = md5($model->password);
					$user->created_at = date('Y-m-d h:i:s');
					$user->save();
					$msg = 'User Registered Successfully';
				}
				else{
					$msg = 'Password and confirm password should be same';
				}
			}
			else{
				$msg = 'User already registered with this email id';
			}

			return $msg;
			
		}


		public static function checkUserRegisterd($email) {
			Yii::log("email 111 ".$email);
			$isRegistered = false;
			if($email!="") {
				Yii::log("email 112 ".$email);
				$user = User::model()->findByAttributes(array('email'=>$email));
				if($user) {
					Yii::log("email 113 ".$email);
					$isRegistered = true;
				}
			}

			return $isRegistered;
		}

		public static function authenticateUser($model) {
			$validUser = false;
			$info = array();
			$user = User::model()->findByAttributes(array('email'=>$model->email,'password'=>md5($model->password)));
			if($user) {
				$user->clientDetails = $model->clientDetails;
				Util::setSessionActive($user);
				$validUser = true;
				$msg="User Login Successfully";
				array_push($info, $msg);
				array_push($info, 1);

			}
			else{
				$msg="Invalid Username or Password";
				array_push($info, $msg);
				array_push($info, 0);
			}

			return $info;
		}

		public static function setSessionActive($user) {
			$userSession = UserSession::model()->findByAttributes(array('session_id'=>session_id()));
			if($userSession) {
				$userSession->user_id = $user->id;
				$userSession->session_id = session_id();
				//$userSession->os = $user->clientDetails;
				$browserInfoUtil = new BrowserInfo;
				$userSession->os = $browserInfoUtil->showInfo('browser');
				$userSession->ip_address = $_SERVER['REMOTE_ADDR'];
				$userSession->isActive = 1;
				if($userSession->update()) {
					Yii::app()->session['user'] = $userSession->id;
					yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
				}
			}
			else{
				$userSession = new UserSession;
				$userSession->user_id = $user->id;
				$userSession->session_id = session_id();
				//$userSession->os = $user->clientDetails;
				$browserInfoUtil = new BrowserInfo;
				$userSession->os = $browserInfoUtil->showInfo('browser');
				$userSession->ip_address = $_SERVER['REMOTE_ADDR'];
				$userSession->isActive = 1;
				if($userSession->save()) {
					Yii::app()->session['user'] = $userSession->id;
					yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
				}
			}
			
		}

		public static function destroySession($sessionId) {
			
			$current_session_id = session_id();
			session_commit();

			// 3. hijack then destroy session specified.
			session_id($sessionId);
			session_start();
			session_destroy();
			session_commit();

			// 4. restore current session id. If don't restore it, your current session will refer     to the session you just destroyed!
			session_id($current_session_id);
			session_start();
			session_commit();
		}



	}
?>