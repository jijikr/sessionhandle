<?php

class UserController extends Controller
{

	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',// we only allow deletion via POST request
        );
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	public function actionSignup() {
		try {
			$msg="";
			$model = new User;
			$model->setScenario('signup');
			if(isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if($model->validate()) {
					$msg = Util::registerUser($model);
					Yii::app()->user->setFlash('updatemsg', $msg); 
					$this->render('signup',array('model'=>$model));
				}
				else{
					$this->render('signup',array('model'=>$model));
				}
			}
			else{
				$this->render('signup',array('model'=>$model));
			}
		}
		catch(Exception $ex) {
			Yii::log("Exception in action signup ".$ex->getMessage());
			$this->render('index');
		}
	}


	public function actionLogin() {
		try {
			$msg="";
			$model = new User;
			$model->setScenario('login');
			if(isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if($model->validate()) {
					$info = Util::authenticateUser($model);
					if($info[1]==1) {
						Yii::app()->user->setFlash('updatemsg', $info[0]); 
						$this->render('index');
					}
					else{
						Yii::app()->user->setFlash('updatemsg', $info[0]); 
						$this->render('login',array('model'=>$model));
					}
					
				}
				else{
					$this->render('login',array('model'=>$model));
				}
			}
			else{
				$this->render('login',array('model'=>$model));
			}
		}
		catch(Exception $ex) {
			Yii::log("Exception in action signup ".$ex->getMessage());
			$this->render('index');
		}
	}

	public function actionLogout()  {
		$sessionId = Yii::app()->session['user'];
		unset(Yii::app()->session['user']);
		Yii::app()->session->clear();
		Yii::app()->session->destroy();
		$session = UserSession::model()->findByPk($sessionId);
		if($session) {
			$session->isActive =0;
			$session->update();
		}
		$this->render('index');
	}

	public function actionListSession() {
		try{
			$userSession = new UserSession;
			if(isset(Yii::app()->session['user'])) {
				$userSess = UserSession::model()->findByPK(Yii::app()->session['user']);
				if($userSess) {
					$user= User::model()->findByPK($userSess->user_id);
					$userSession = UserSession::model()->findAllByAttributes(array('user_id'=>$userSess->user_id,'isActive'=>1));
					if($userSession) {
						$this->render('listSession',array('userSessions'=>$userSession,'email'=>$user->email));
					}
					else{
						Yii::app()->user->setFlash('updatemsg', "No active sessions in your login"); 
						$this->render('index');
					}
				}
				else{
					Util::destroySession(session_id());
					Yii::app()->user->setFlash('updatemsg', "Please login to see your active sessions"); 
					$this->render('index');
				}
				
				
			}
			else{
				Yii::app()->user->setFlash('updatemsg', "Please login to see your active sessions"); 
				$this->render('index');
			}
			
		}
		catch(Exception $ex) {
			Yii::log("Exception in action signup ".$ex->getMessage());
			$this->render('index');
		}
	}

	public function actionClearSession() {
		try{
			$sessionId = isset($_REQUEST['session_id'])?$_REQUEST['session_id']:"";
			if($sessionId!="") {
				
				$userSession = UserSession::model()->findByAttributes(array('session_id'=>$sessionId,'isActive'=>1));
				$userSession->isActive=0;
				if($userSession->update()){
					Util::destroySession($userSession->session_id);
				}
				
				
				$this->redirect('ListSession');
			}
			else{
				
				$session = UserSession::model()->findByPK(Yii::app()->session['user']);
				
				$sessions = UserSession::model()->findAllByAttributes(array('user_id'=>$session->user_id));
				
				if($sessions) {
					
					foreach ($sessions as $sess) {
						
						$sess->isActive=0;
						if($sess->update()){
						
							Util::destroySession($sess->session_id);
						}
					}
					
					Yii::app()->user->setFlash('updatemsg', "All sessions destroyed"); 
					
					$this->render('index');
				}
			}
		}
		catch(Exception $ex) {
			Yii::log("Exception in action signup ".$ex->getMessage());
			$this->render('index');
		}
	}

	/* Api for getting maximum evenly distributed investment option for any amount by satisfying the minimum condition*/
	public function actionGetInvestmentDistribution() {
		$distribution = array();
		$distributionList =array();
		$policyCompo = array();
		$contraintOption = array();
		$finalList = array();
		$amount = isset($_POST['amount']) ? $_POST['amount'] : 0;
		
		$constraints = isset($_POST['invest']) ? $_POST['invest'] : 0;
		if($amount>0) {
			$minCount = $this->getPolicyStatisfyCount($constraints,$amount);
			/*This if part will equally divide the amount to each policy, if they satisfy the min. constrain condition*/
			if( ($minCount == count($constraints)) && ($amount %count($constraints) == 0)) {
				$investValue = $amount/count($constraints);
				foreach ($constraints as $key => $value) {
					$distribution[$key] = $investValue;
				}
				array_push($distributionList, $distribution);

			}
			else{
				$policyCompo = $this->pc_permute(array_keys($constraints));
				/* This for loop will give the min.constraint amount to each policy in all the permutation of policies*/
				foreach ($policyCompo as $policies) {
					$distribution = $this->getInvestMent($policies,$constraints,$amount);
					$distributedAmount = array_sum(array_values($distribution));				
					$validCount =0;
					foreach ($distribution as $policy => $value) {
						if($value > 0) {
							$validCount = $validCount+1;
						}
					}
					/* This code block will distribute evenly the rest amount to each policy in the policy array, which is sorted in  ascending order of assigned amount */
					if($validCount>0) {
						asort($distribution);							
						$nextDistribution = $amount- $distributedAmount;							
						while($nextDistribution>0)	{
							$investArr = array_keys($distribution);
							$amountArr = array_values($distribution);								
							for($i=0;$i<count($amountArr)-1;$i++) {
								$firstAmount = $amountArr[$i];
								$nextAmount = $amountArr[$i+1];
								if($firstAmount> 0 && ($nextAmount) > 0 ) {
									if( $firstAmount<$nextAmount) {											
										if($nextDistribution >= $nextAmount-$firstAmount){								
											$nextDistribution = $nextDistribution- ($nextAmount-$firstAmount); 
											$firstAmount = $firstAmount + ($nextAmount-$firstAmount);				
										}
										else{
											$firstAmount = $firstAmount + $nextDistribution;
											$nextDistribution =0;
												
										}
									}
									else{
										$firstAmount = $firstAmount + $nextDistribution;
										$nextDistribution =0;
											
									}
										
								}								
								$distribution[$investArr[$i]] = $firstAmount;									
							}
								
						}							

					}
						
					/* This code block will create a hash map, with key is the count of the policies, which has greater than 0 amount investment and value will be array of those policies*/
					if(array_key_exists($validCount, $finalList)) {
						$arr = array();
						$arr = $finalList[$validCount];
						if(!in_array($distribution, $arr)) {
							array_push($arr, $distribution);
						}							
						$finalList[$validCount] = $arr;
					}
					else{
						$arr = array();
						array_push($arr, $distribution);
						$finalList[$validCount] = $arr;
					}
						
					
							
				}
				$distributionList = $this->getFinalInvestCombo($finalList);		
			}
				
			

			
			echo json_encode($distributionList);
		}
		else{
			echo 'Please enter valid inputs !!!!';
		}
		
	}

	public function getPolicyStatisfyCount($constraints,$amount) {
		$count =0;
		foreach ($constraints as $key => $value) {
			if($value <$amount) {
				$count = $count+1;
			}
		}

		return $count;
	}

	/* This function will give the permutation of all the policies*/
	public function pc_permute($items, $perms = [],&$ret = []) {
	   if (empty($items)) {
	       $ret[] = $perms;
	   } else {
	       for ($i = count($items) - 1; $i >= 0; --$i) {
	           $newitems = $items;
	           $newperms = $perms;
	           list($foo) = array_splice($newitems, $i, 1);
	           array_unshift($newperms, $foo);
	           $this->pc_permute($newitems, $newperms,$ret);
	       }
	   }
	   return $ret;
	}

	public function getInvestMent($policies,$constraints,$amount) {
		$investments = array();
		foreach ($policies as $policy) {
			if($constraints[$policy]<= $amount) {
				$investment = $constraints[$policy];
				$amount = $amount - $investment;
				$investments[$policy] = $investment;
			}
			else{
				$investments[$policy] = 0;
				
			}
			unset($constraints[$policy]);
			
			
		}

		return $investments;
	}

	/* This function first take the policy option , which has maximum no.of polocies having investment money and then among that, it will take the most evenly distributed policy option*/
	public function getFinalInvestCombo($finalList) {		
		krsort($finalList);
		$maxKey = max(array_keys($finalList));
		$finalInvestArr = $finalList[$maxKey];
		$investHasMap = array();
		foreach ($finalInvestArr as $finalInvest) {
			$diff =0;
			foreach ($finalInvest as $policy => $amount) {
				$diff = abs($diff)-$amount;
			}
			$diff = abs($diff);
			if(array_key_exists($diff,$investHasMap)){
				$arr = $investHasMap[$diff];
				array_push($arr, $finalInvest);
				$investHasMap[$diff] = $arr;
			}
			else{
				$arr = array();
				array_push($arr, $finalInvest);
				$investHasMap[$diff] = $arr;
			}

		}
		$minDiff = min(array_keys($investHasMap));
		return $investHasMap[$minDiff];
	}

}

?>