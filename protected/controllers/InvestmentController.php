<?php

class Investment extends Controller
{

	public function actionGetInvestmentDistribution() {
		$distribution = array();
		$distributionList =array();
		$policyCompo = array();
		$contraintOption = array();
		$amount = isset($_REQUEST['amount']) ? isset($_REQUEST['amount']) : 0;
		$constraints = isset($_REQUEST['invest']) ? isset($_REQUEST['invest']) : 0;
		if($amount!=0 && $constraints !=0) {
			$policyCompo = $this->pc_permute(array_keys($constraints),$policyCompo);
			foreach ($policyCompo as $policies) {
				foreach ($policies as $policy) {
					$contraintOption[$policy] = $constraints[$policy];
				}

				$distribution = $this->getInvestMent($contraintOption,$amount);
				$distributedAmount = array_sum(array_values($distribution));
				if($distributedAmount<$amount) {
					$nextDistribution = count($distribution)/($amount-$distributedAmount);
					foreach ($distribution as $policy => $value) {
						if($value!=0) {
							$value += $nextDistribution;
							$distribution[$policy] = $value;
						}
					}

				}

				array_push($distributionList, $distribution);
			}
			$distributionList = $this->getFinalInvestCombo($distributionList);
			echo json_decode($distributionList);
		}
		else{
			echo 'Please enter valid inputs !!!!';
		}
		
	}


	public function pc_permute($items, $perms = array()) {
	    if (empty($items)) { 
	        
	    } else {
	        for ($i = count($items) - 1; $i >= 0; --$i) {
	             $newitems = $items;
	             $newperms = $perms;
	             list($foo) = array_splice($newitems, $i, 1);
	             array_unshift($newperms, $foo);
	             pc_permute($newitems, $newperms);
	         }
	    }

	    return $perms;
	}

	public function getInvestMent($constraints,$amount) {
		$investmenta = array();
		foreach ($constraints as $policy => $minAmount) {
			if($minAmount< $amount) {
				$investment = $minAmount;
				$amount = $amount - $minAmount;
				$investments[$policy] = $investment;
			}
			else{
				$investments[$policy] = 0;
				
			}
			unset($constraints[$policy]);
			
			
		}

		return $investments;
	}

	public function getFinalInvestCombo($distributionList) {
		$finalList =array();
		foreach ($distributionList as $distribution) {
			$count=0;
			foreach ($distribution as $policy => $amount) {
				if($amount>0) {
					$count = $count++;
				}
			}

			$finalList[$count] =  $distribution;
		}

		return krsort($finalList);
	}
}
?>